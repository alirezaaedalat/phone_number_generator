const arr = [12, 35, 36];

function rnd_numb() {
    let number = Math.floor((Math.random() * 10000000) + 1);
    let code = arr[Math.floor(Math.random() * arr.length)]

    return (number.toString().length == 6) ? "09" + code + "0" + number : "09" + code + number;
}

pm.test("get numb List :)", function () {
    pm.environment.set("numb_list", rnd_numb());
   
    pm.expect(pm.environment.get("numb_list").length).to.equal(11);
});